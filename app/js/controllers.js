var projectControllers = angular.module('projectControllers', []);

//CONTROLLER : List of projects
projectControllers.controller('ProjectListCtrl', ['$scope', '$http',
  function ($scope, $http) {
      $http.get('projects.json').success(function(data) { //Load the JSON file
      $scope.projects = data.project; //load all the projects in the scope --> scope : reachable in the view
    });

  }]);

//CONTROLLER : Details of one project
projectControllers.controller('ProjectDetailCtrl', ['$scope', '$routeParams','$http',
  function($scope, $routeParams, $http) {
      $http.get('projects.json').success(function(data) {
      $scope.project = data.project[$routeParams.projectId -1]; //Load the selected project in the scope
    });

  }]);




