var projectApp = angular.module('projectApp', ['ngRoute', 'projectControllers']); //New module with its dependencies

//Model View Controller : ROUTING CONFIGURATION
projectApp.config(['$routeProvider',
  function($routeProvider){
    $routeProvider.
    // when('/', {//At the root of the website --> load the view projet-list and the controller List
    //   templateUrl: 'project/project-list.html',
    //   controller: 'ProjectListCtrl'
    // }).
    when('/projects/:projectId', {//According to the selected project : URL route with variable
      templateUrl: 'project/project-detail.html#section_projects',//Link to an anchor to stay in the good area on the page
      controller: 'ProjectDetailCtrl',
    }).
    otherwise({
      // redirectTo: ''
      templateUrl: 'project/project-list.html',
      controller: 'ProjectListCtrl'
    });

  }]);