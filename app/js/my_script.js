$(document).ready(function()
{
	//NAVIGATION - SCROLL ANIMATION
	var linksMenu = $('.menu a'); //All the links of the menu

	//When there is a click on one item of the menu
	linksMenu.on('click', function () {
		$('.menu').addClass('closed');
		$('.text-menu').html($('.text-menu').text() == 'Fermer menu' ? 'Menu' : 'Fermer menu');//Change the text at the bottom of the menu according to its state (close or open)
		var link=$(this); //Recover the clicked link
		var href = link.attr('href'); //Find the attribute href
		var go = $(href).offset().top - 50;//Work out the postion where to go and -50 : height of the menu
		
		//ANIMATION
		$('body, html').stop().animate({
			scrollTop: go
		}, 2000); 

		return false;		
	});

	//HAMBURGER MENU
	$('.open-menu').on('click', function () {
		//toggle = remove or add the class 'closed' according the situation
		$('.menu').toggleClass('closed');
		$('.text-menu').html($('.text-menu').text() == 'Fermer menu' ? 'Menu' : 'Fermer menu');//Change the text at the bottom of the menu according to its state (close or open)
	});

	
	//HANDLE ACTIVE ITEM IN THE MENU
	var links = $('.menu a');//All the links of the menu
	var linkSections = links.map(function(){ 
		var href = $(this).attr('href');
		return $(href);
	});

	var lastId;
	
	$(document).on('scroll', function(){ //When the user is scrolling

		var target = $(this).scrollTop() + $(window).height()/2;//Middle of the page
		var current;
		
		linkSections.each(function(){ //Find the active section
			var top = $(this).offset().top;
			if(top < target)
			{
				current = this;
			}
		});		

		var id = current[0].id;

		//When the user is scrolling
		if(lastId !== id)
		{
			lastId = id;
			links.removeClass('active'); //Remove 'active' of all the links
			links.filter('[href=#'+id+']').addClass('active');//And add the class active at the active link 
		}
	});

	

	//PARALLAX EFFECT
	var img = $('.parallax');

	$(document).on('scroll', function(){//When the user is scrolling
		var target = $(this).scrollTop();// Recover the position		
		var current;
		
		linkSections.each(function(){//Find the active section
			var top = $(this).offset().top;
			if(top < target)
			{
				current = this;
			}
		});		

		var id = current[0].id;
		var target = -target/6;
		var position = '50%'+target+'px';
		$(img).css("background-position", position); //Change the position of the background image according the scroll
	});


	//SKILLS PUZZLE
	//According the area (position of the mouse) --> adjust the text displayed
	$("#skills").mouseover(function () {
		$('.content h4').text("Connaissances :");
		$('.content p').text("WordPress, Adobe InDesign, Adobe Premiere, Blender, Open GL, AngularJS");
	});

	$("#graphic").mouseover(function () {
		$('.content h4').text("Graphisme :");
		$('.content p').text("Adobe Illustrator, Adobe Photoshop, Adobe After Effects");	
	});

	$("#prog").mouseover(function () {
		$('.content h4').text("Programmation :");
		$('.content p').text("C++, HTML5, CSS3, LESS, Java Script, jQuery, PHP, MySQL");	
	});

	$("#other").mouseover(function () {
		$('.content h4').text("Langues :");
		$('.content p').text("Anglais oral et écrit");
	});

	$(".my_skills").mouseout(function () { //Default text (when the mouse isn't on a piece of the puzzle)
		$('.content h4').text("Survolez les zones du puzzle pour découvrir mes compétences.");
		$('.content p').text("");
	});


	//FORM WITH AJAX
	$('#envoyer').click(function(e) {
		var mail = false;
		var name = false;
		var message = false;
		e.preventDefault();
		//Check the inputs
		if($('.nom').val() == ''){ 
			$('.nom').css({
				border:'1px solid red'
			})
			name = false; 
		} else {
				$('.nom').css({
				border:''
		})
			name = true;
		} if ($('.email').val() == ''){
			$('.email').css({
				border:'1px solid red'
			})
			email = false;
		} else {
			$('.email').css({
			border:''
		})
			email = true;
		} if ($('.message').val() == '') {
			$('.message').css({
				border:'1px solid red'
			})
			message = false;
		} else {
			$('.message').css({
			border:''
		}) 
			message = true;
	   } 
	   //If all the inputs are full
	   if (message == true && email == true && name == true) {
        $.ajax({
               type:"post",
               url:"http://mariebenoist.fr/testpf/mail.php",//Redirect to the PHP to send the mail
               data:  $("#form").serialize(),
               success: function(response){
                   $('.success').addClass('show-success');//Add a class to the CSS to display a message
                   $("#form")[0].reset();//Reset the inputs of the form
               }
         });
        }
      });
})