module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    less:{
      dev:{
        files:[{
          dest:"app/styles/main.css",
          src:"app/styles/main.less"
        }]
      }
    },

    uglify: { //Minify JS files
      build: {
        files: {
          'app/js/main.min.js': ['app/js/app_projects.js','app/js/controllers.js','app/js/my_script.js'],
          'app/bower_components/angular/angular-route.min.js': ['app/bower_components/angular/angular-route.js']
        }
      }
    },

    cssmin: {//Minify CSS file
      build: {
        files: {
          'app/styles/main.min.css': ['app/styles/main.css']
        }
      }
    },

    htmlmin: { //Minify html file
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      build: {
        files: {
          'app/index-prod.html': ['app/index-dev.html']
        }
      }
    },

    autoprefixer:{
      dev:{
        files:{
          "app/styles/main.css":"app/styles/main.css"
        }
      }
    },

    connect:{
      server:{
        options:{
          base:['app'],
          livereload:35729 
        }
      }
    },

    watch:{
      less:{
          files:['app/styles/*.less'],//When a LESS file has changed
          tasks:['css']//Refreshing the CSS
        },

        livereload:{
        files:['app/index.html', 'app/styles/*.css'],//When a CSS or HTML file has changed
        options:{
          livereload:true //Refrishing 
        }
      }

    },


  });

  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  
  
  // Default tasks
  grunt.registerTask('default', ['server']);
  grunt.registerTask('css', ['less','autoprefixer']);
  grunt.registerTask('server', ['css','connect','watch']);
  grunt.registerTask('build', ['less', 'uglify','cssmin','htmlmin']);

};